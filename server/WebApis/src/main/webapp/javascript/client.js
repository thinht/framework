$(function() {
	"use strict";

	var content = $('#content');
	var input = $('#input');
	var status = $('#status');
	var myName = false;
	var author = null;
	var logged = false;
	var socket = atmosphere;

	var request = {
	//	url : 'http://localhost:8080/chat',
		url : 'http://localhost:8080/webapi/chat',
		contentType : "application/json",
		//logLevel : 'debug',
		transport : 'websocket',
		trackMessageLength : true,
		fallbackTransport : 'long-polling'
	};

	request.onOpen = function(response) {
		content.html($('<p>', {
			text : 'Atmosphere connected using ' + response.transport
		}));
	};

	var channelId = "";
	var from = '';
	var to = '';

	request.onMessage = function(response) {
		var message = response.responseBody;
		console.log(message);
		var json = atmosphere.util.parseJSON(message);

		if (json.status == "READY") {
			$('#register').attr('style', 'display: none;')
			$('#chat').attr('style', 'display: block;')
			from = json.to;
			to = json.from;
		} else if (json.status == 'ONCHAT') {
			addMessage(json.message)
		}
	};

	request.onClose = function(response) {
		logged = false;
	}

	request.onError = function(response) {
		content.html($('<p>', {
			text : 'Sorry, but there\'s some problem with your '
					+ 'socket or the server is down'
		}));
	};

	var subSocket = socket.subscribe(request);

	$('#btnSubmit').click(function() {
		var name = $('#name').val();
		var email = $('#email').val();
		var firstName = $('#firstName').val();
		var lastName = $('#lastName').val();
		var message = {
			name : name,
			email : email,
			firstName: firstName,
			lastName: lastName
		};

		subSocket.push(atmosphere.util.stringifyJSON(message));
	});

	$('#btnSubmitChat').click(function() {
		var message = $('#message').val();
		var message = {
			from : from,
			to : to,
			message : message,
			status : 'ONCHAT'
		};
		console.log(message);
		subSocket.push(atmosphere.util.stringifyJSON(message));

		$('#message').val('');
	});

	var confirmDialog = $('#dialogConfirmControl');
	function showConfirmDialog(msg) {
		var deferred = Q.defer();

		confirmDialog.find('.modal-title').text('Confirmation');
		confirmDialog.find('.modal-body p').html(msg);

		$('#btnOk').click(function() {
			deferred.resolve(true);
		});

		$('#btnCancel').click(function() {
			// confirmDialog.modal('hide');
			deferred.resolve(false);
		});

		confirmDialog.modal('show');

		return deferred.promise;
	}
	;

	function addMessage(message) {
		content.append('<p>' + message + '</p>');
	}

});
