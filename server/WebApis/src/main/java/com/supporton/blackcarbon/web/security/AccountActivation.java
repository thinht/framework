//package com.supporton.blackcarbon.web.security;
//
//import java.io.IOException;
//
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.util.Assert;
//import org.springframework.web.context.support.SpringBeanAutowiringSupport;
//
//import com.supporton.blackcarbon.business.service.UserManagementService;
//
///**
// * @author thinhtat
// *
// */
//public class AccountActivation extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//	private static final Logger logger = Logger.getLogger(AccountActivation.class);
//
//	@Autowired
//	private UserManagementService userManagementService;
//
//	@Autowired
//	private Environment env;
//
//	public void init(ServletConfig config) throws ServletException {
//		super.init(config);
//
//		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
//
//		Assert.notNull(userManagementService, "Service must be not null");
//		Assert.notNull(env, "Environment must be not null");
//	}
//
//	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//		logger.info("activate account");
//
//		// Handle update account
//		String token = req.getParameter("activationCode");
//		if (token != null) {
//			userManagementService.verifyToken(token);
//		}
//		// redirect to client
//		res.setContentType("text/html");
//		res.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
//		res.setHeader("Location", env.getProperty("CLIENT_DOMAIN_URL"));
//	}
//}
