//package com.supporton.blackcarbon.web.decorators;
//
//import org.modelmapper.ModelMapper;
//
//import com.fasterxml.jackson.annotation.JsonFilter;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.supporton.blackcarbon.business.dto.UserDTO;
//import com.supporton.blackcarbon.web.utils.ResponseViewFilter;
//
//@JsonFilter("myFilter")
//public class UserAuthenticationDecorator extends UserDTO {
//	private ResponseViewFilter<UserAuthenticationDecorator> filter;
//
//	public UserAuthenticationDecorator(UserDTO userDto) {
//		ModelMapper mapper = new ModelMapper();
//		mapper.map(userDto, this);
//		filter = new ResponseViewFilter<UserAuthenticationDecorator>();
//	}
//
//	public String decorate() throws JsonProcessingException {
//		// removed fields
//		String[] ignoreFields = new String[] { "id", "password", "email" };
//		return this.filter.filter(this, ignoreFields, "myFilter");
//	}
//}
