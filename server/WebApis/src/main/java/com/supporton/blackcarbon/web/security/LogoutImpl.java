package com.supporton.blackcarbon.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @author thinhtat
 * 
 */
public class LogoutImpl implements LogoutSuccessHandler {
	private JdbcTokenStore tokenStore;

	public JdbcTokenStore getTokenstore() {
		return tokenStore;
	}

	public void setTokenstore(JdbcTokenStore tokenStore) {
		this.tokenStore = tokenStore;
	}

	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null) {
			String tokenValue = authHeader.replace("Bearer", "").trim();
			OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
			tokenStore.removeAccessToken(accessToken);

			response.getWriter().write("Logged out successfully.");
		} else {
			response.getWriter().write("Logged out failure.");
		}

	}

}
