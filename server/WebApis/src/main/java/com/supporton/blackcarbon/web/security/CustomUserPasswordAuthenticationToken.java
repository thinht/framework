package com.supporton.blackcarbon.web.security;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.supporton.blackcarbon.model.auth.UserAuthentication;

/**
 * @author thinhtat
 * 
 */
public class CustomUserPasswordAuthenticationToken extends AbstractAuthenticationToken {
	private static final long serialVersionUID = 310L;
	private final Object principal;
	private Object credentials;
	private UserAuthentication userAuthentication;

	public CustomUserPasswordAuthenticationToken(Object principal, Object credentials) {
		super(null);
		this.principal = principal;
		this.credentials = credentials;
		setAuthenticated(false);
	}

	public CustomUserPasswordAuthenticationToken(Object principal, Object credentials,
			Collection<? extends GrantedAuthority> authorities, UserAuthentication userAuthentication) {
		super(authorities);
		this.principal = principal;
		this.credentials = credentials;
		this.userAuthentication = userAuthentication;
		super.setAuthenticated(true);
	}

	public Object getCredentials() {
		return this.credentials;
	}

	public Object getPrincipal() {
		return this.principal;
	}

	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		if (isAuthenticated) {
			throw new IllegalArgumentException(
					"Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
		}

		super.setAuthenticated(false);
	}

	public void eraseCredentials() {
		super.eraseCredentials();
		this.credentials = null;
	}

	@Override
	public String toString() {
		return userAuthentication.getUsername();
	}
}
