//package com.supporton.blackcarbon.web.exception;
//
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.core.Response.Status;
//import javax.ws.rs.ext.ExceptionMapper;
//import javax.ws.rs.ext.Provider;
//
//import org.hibernate.exception.ConstraintViolationException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.dao.DataIntegrityViolationException;
//
//import com.supporton.blackcarbon.business.dto.ResultDTO;
//import com.supporton.blackcarbon.business.exceptions.DataValidationException;
//import com.supporton.blackcarbon.business.exceptions.UserNotFoundException;
//import com.supporton.blackcarbon.business.utils.BusinessServiceConstants;
//
///**
// * @author thinhtat
// * 
// */
//public class ApplicationException {
//  private static final Logger logger = LoggerFactory.getLogger(ApplicationException.class);
//
//  @Provider
//  public static class PageNumberFormatException implements ExceptionMapper<NumberFormatException> {
//    public Response toResponse(NumberFormatException ex) {
//      logger.info("Wrong format for page number.");
//
//      return Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
//          .entity("Wrong format for page number.").build();
//    }
//  }
//
//  @Provider
//  public static class DataValidationExceptionMapper implements
//      ExceptionMapper<DataValidationException> {
//    public Response toResponse(DataValidationException ex) {
//      return Response
//          .status(Status.BAD_REQUEST)
//          .type(MediaType.APPLICATION_JSON)
//          .entity(
//              new ResultDTO(BusinessServiceConstants.RESULT_ERROR,
//                  BusinessServiceConstants.RESULT_ERROR_INPUT_VALIDATION)).build();
//    }
//  }
//
//  @Provider
//  public static class ConstraintVoliationExceptionMapper implements
//      ExceptionMapper<ConstraintViolationException> {
//    public Response toResponse(ConstraintViolationException ex) {
//      logger.info("DataValidationException.");
//
//      return Response
//          .status(Status.BAD_REQUEST)
//          .type(MediaType.APPLICATION_JSON)
//          .entity(
//              new ResultDTO(BusinessServiceConstants.RESULT_ERROR,
//                  "DataValidationException.")).build();
//    }
//
//  }
//  @Provider
//  public static class DataIntegrityViolationExceptionMapper implements
//      ExceptionMapper<DataIntegrityViolationException> {
//    public Response toResponse(DataIntegrityViolationException ex) {
//      logger.info("DataValidationException.");
//
//      return Response
//          .status(Status.BAD_REQUEST)
//          .type(MediaType.APPLICATION_JSON)
//          .entity(
//              new ResultDTO(BusinessServiceConstants.RESULT_ERROR,
//                  "DataIntegrityViolationException.")).build();
//    }
//
//  }
//  @Provider
//  public class ThrowableMapper implements ExceptionMapper<Throwable> {
//    @Override
//    public Response toResponse(Throwable e) {
//      return Response
//          .status(Status.INTERNAL_SERVER_ERROR)
//          .type(MediaType.APPLICATION_JSON)
//          .entity(
//              new ResultDTO(BusinessServiceConstants.RESULT_ERROR,
//                  BusinessServiceConstants.RESULT_ERROR_INTERNAL_SERVER_ERROR)).build();
//    }
//  }
//
//  @Provider
//  public static class UserNotFoundExceptionMapper implements ExceptionMapper<UserNotFoundException> {
//    public Response toResponse(UserNotFoundException ex) {
//      return Response
//          .status(Status.BAD_REQUEST)
//          .type(MediaType.APPLICATION_JSON)
//          .entity(
//              new ResultDTO(BusinessServiceConstants.RESULT_ERROR,
//                  BusinessServiceConstants.RESULT_ERROR_USER_NOT_FOUND)).build();
//    }
//  }
//}
