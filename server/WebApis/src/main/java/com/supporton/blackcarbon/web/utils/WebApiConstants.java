package com.supporton.blackcarbon.web.utils;

/**
 * @author thinhtat
 *
 */
public class WebApiConstants {
	public static final String RESULT_STATUS_SUCCESS = "success";
	public static final String RESULT_STATUS_ERROR = "error";

	// Contact
	public static final String CONTACT_CREATE_SUCCESS = "Contact created successfully";
	public static final String CONTACT_UPDATE_SUCCESS = "Contact updated successfully";
	public static final String CONTACT_DELETE_SUCCESS = "Contact deleted successfully";
	public static final String IMAGE_UPLOAD_SUCCESS = "Image uploaded successfully";
	public static final String IMAGE_UPLOAD_FAILURE = "Image uploaded failure";
	
	public static final String CUSTOMER_DEFAULT_PASSWORD = "123123";
}
