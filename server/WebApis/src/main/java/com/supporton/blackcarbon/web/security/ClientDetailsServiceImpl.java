package com.supporton.blackcarbon.web.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Service;

/**
 * @author thinhtat
 * 
 */
@Service
public class ClientDetailsServiceImpl implements ClientDetailsService {

	private static final String CLIENT_ID = "clientId";
	private static final String SECRET = "secret";

	public ClientDetails loadClientByClientId(String clientId) throws OAuth2Exception {

		BaseClientDetails clientDetails = new BaseClientDetails();

		List<String> authorizedGrantTypes = new ArrayList<String>();
		authorizedGrantTypes.add("password");
		authorizedGrantTypes.add("refresh_token");
		authorizedGrantTypes.add("client_credentials");

		clientDetails.setClientId(CLIENT_ID);
		clientDetails.setClientSecret(SECRET);
		clientDetails.setAuthorizedGrantTypes(authorizedGrantTypes);

		return clientDetails;
	}

}
