//package com.supporton.blackcarbon.web.security;
//
//import java.io.IOException;
//
//import javax.ws.rs.container.ContainerRequestContext;
//import javax.ws.rs.container.ContainerResponseContext;
//import javax.ws.rs.container.ContainerResponseFilter;
//import javax.ws.rs.core.MultivaluedMap;
//
///**
// * @author thinhtat
// *
// */
//public class CorsResponseFilter implements ContainerResponseFilter {
//	/**
//	 * Add the cross domain data to the output if needed
//	 * 
//	 * @param creq
//	 *            The container request (input)
//	 * @param cres
//	 *            The container request (output)
//	 * @return The output request with cross domain if needed
//	 */
//	@Override
//	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
//			throws IOException {
//		MultivaluedMap<String, Object> headers = responseContext.getHeaders();
//		headers.add("Access-Control-Allow-Origin", "*");
//		headers.add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
//		headers.add("Access-Control-Allow-Credentials", "true");
//		headers.add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
//		headers.add("Access-Control-Max-Age", "1209600");
//	}
//}
