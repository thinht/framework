package com.supporton.blackcarbon.web.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.supporton.blackcarbon.auth.exception.NotFoundException;
import com.supporton.blackcarbon.auth.service.AuthenticateService;
import com.supporton.blackcarbon.model.auth.Role;
import com.supporton.blackcarbon.model.auth.UserAuthentication;

/**
 * @author thinhtat
 * 
 */
@Component
public class CustomUserAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private AuthenticateService authenticateService;

	@Override
	public CustomUserPasswordAuthenticationToken authenticate(Authentication authentication)
			throws AuthenticationException {
		if (authentication != null && authentication.getPrincipal() != null
				&& authentication.getCredentials() != null) {

			// Encode Base64
			String username = new String(Base64.decodeBase64(authentication.getName()));
			String password = new String(Base64.decodeBase64(authentication.getCredentials().toString()));

			try {
				// authenticate
				UserAuthentication userAuthentication = authenticateService.authenticate(username, password);
				List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

				for (Role role : userAuthentication.getRole()) {
					grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
				}

				// clear credential if any and then return
				CustomUserPasswordAuthenticationToken authToken = new CustomUserPasswordAuthenticationToken(username,
						password, grantedAuthorities, userAuthentication);

				return authToken;
			} catch (NotFoundException e) {
				throw new BadCredentialsException(e.getMessage());
			}
		}
		throw new BadCredentialsException("Invalid username and password, please try again!");
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}

}
