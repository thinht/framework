package com.supporton.blackcarbon.web.application;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

public class ApplicationDatabaseInitializer implements ServletContextListener {
	private final Logger logger = LoggerFactory.getLogger(ApplicationDatabaseInitializer.class);
	private static final String MASTER_DATA_CRIPTS = "master-data.sql";

	@Override
	public void contextDestroyed(ServletContextEvent event) {

	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		logger.info("Preparing database...........");
		ServletContext servletContext = event.getServletContext();

		Properties env = new Properties();
		try {

			InputStream inStream = servletContext.getClassLoader().getResourceAsStream("data-source.properties");
			env.load(inStream);

			Class.forName(env.getProperty("jdbc.driverClassName"));
			Connection connection = DriverManager.getConnection(env.getProperty("jdbc.url"),
					env.getProperty("jdbc.username"), env.getProperty("jdbc.password"));

			ScriptUtils.executeSqlScript(connection, new ClassPathResource(MASTER_DATA_CRIPTS));

			logger.info("Preparing database...........DONE!");
		} catch (IOException e) {
			logger.info("Can't create master database...........DONE!Please insert master data!");
		} catch (ClassNotFoundException e) {
			logger.info("Can't create master database...........DONE!Please insert master data!");
			e.printStackTrace();
		} catch (SQLException e) {
			logger.info("Can't create master database...........DONE!Please insert master data!");
			e.printStackTrace();
		} catch (Exception e) {
			logger.info("Can't create master database...........DONE!Please insert master data!");
			e.printStackTrace();
		}
	}

}
