package com.supporton.blackcarbon.web.utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

public class ResponseViewFilter<T> {

	public String filter(T value, String[] fields, String filterQualifier)
			throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		FilterProvider filters = new SimpleFilterProvider()
				.addFilter(filterQualifier,
						SimpleBeanPropertyFilter.serializeAllExcept(fields));
		String json = mapper.writer(filters).writeValueAsString(value);
		return json;
	}
}