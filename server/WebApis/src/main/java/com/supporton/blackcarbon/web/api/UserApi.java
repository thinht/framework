//package com.supporton.blackcarbon.web.api;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.Context;
//import javax.ws.rs.core.MediaType;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.oauth2.provider.OAuth2Authentication;
//
//import com.supporton.blackcarbon.business.dto.UserDTO;
//import com.supporton.blackcarbon.business.exceptions.UserNotFoundException;
//import com.supporton.blackcarbon.business.service.UserManagementService;
//
///**
// * @author thinhtat
// * 
// */
//@Path("/user")
//public class UserApi {
//	@Autowired
//	private UserManagementService userManagementService;
//
//	@Context
//	HttpServletRequest request;
//
//	@GET
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	public UserDTO getUserAuthentication() throws UserNotFoundException {
//
//		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
//				.getAuthentication();
//
//		if (authentication == null) {
//			throw new UserNotFoundException();
//		}
//
//		UserDTO userDTO = userManagementService
//				.findUserByUsername(authentication.getUserAuthentication().getPrincipal().toString());
//		return userDTO;
//	}
//
//}
