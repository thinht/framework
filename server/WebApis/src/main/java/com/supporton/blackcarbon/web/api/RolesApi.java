//package com.supporton.blackcarbon.web.api;
//
//import java.util.List;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.DELETE;
//import javax.ws.rs.GET;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.Context;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.supporton.blackcarbon.business.dto.ResultDTO;
//import com.supporton.blackcarbon.business.dto.RoleDTO;
//import com.supporton.blackcarbon.business.exceptions.DataValidationException;
//import com.supporton.blackcarbon.business.exceptions.NotFoundException;
//import com.supporton.blackcarbon.business.exceptions.UserNotFoundException;
//import com.supporton.blackcarbon.business.service.RolesManagementService;
//import com.supporton.blackcarbon.business.utils.BusinessServiceConstants;
//
///**
// * @author thinhtat
// * 
// */
//@Path("/role")
//public class RolesApi {
//
//	@Autowired
//	private RolesManagementService rolesManagementService;
//
//	@Context
//	HttpServletRequest request;
//
//	@GET
//	@Path("/all/{id}")
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	public Response getRolesById(@PathParam("id") Long id) throws NotFoundException {
//		List<RoleDTO> roleDTO = rolesManagementService.findRolesById(id);
//
//		return Response.status(Response.Status.OK).entity(roleDTO).build();
//	}
//
//	@POST
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	public Response createRole(RoleDTO roleDTO) throws DataValidationException {
//
//		rolesManagementService.createRole(roleDTO);
//
//		return Response.status(Response.Status.CREATED)
//				.entity(new ResultDTO(BusinessServiceConstants.RESULT_SUCCESS, "A role has been created")).build();
//	}
//
//	@DELETE
//	@Path("/all/{id}")
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	public Response deleteRoleById(@PathParam("id") Long id) throws NotFoundException {
//		rolesManagementService.deleteRole(id);
//		return Response.status(Response.Status.OK)
//				.entity(new ResultDTO(BusinessServiceConstants.RESULT_SUCCESS, "A label has been removed successfully"))
//				.build();
//	}
//
//	@PUT
//	@Path("/all/{id}")
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	public Response updateFullLabel(@PathParam("id") Long id, RoleDTO roleDTO)
//			throws DataValidationException, UserNotFoundException, NotFoundException {
//		roleDTO.setId(id);
//		rolesManagementService.updateRole(id, roleDTO);
//
//		return Response.status(Response.Status.OK)
//				.entity(new ResultDTO(BusinessServiceConstants.RESULT_SUCCESS, "A label has been updated successfully"))
//				.build();
//
//	}
//
//}
