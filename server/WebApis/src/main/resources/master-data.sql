use supporton;

INSERT INTO `roles` (id, name, description) 
SELECT * FROM (SELECT 1 ,'SUPER_ADMIN', 'Has global access over the entire accounts of SupportON') AS tmp
WHERE NOT EXISTS (
    SELECT id FROM roles WHERE id = 1
) LIMIT 1;

INSERT INTO `roles` (id, name, description) 
SELECT * FROM ( SELECT 2, 'ADMIN', 'Has global access over this account') AS tmp
WHERE NOT EXISTS (
    SELECT id FROM roles WHERE id = 2
) LIMIT 1;

INSERT INTO `roles` (id, name, description) 
SELECT * FROM (SELECT 3, 'USER', 'Can chat, recieve emails ') AS tmp
WHERE NOT EXISTS (
    SELECT id FROM roles WHERE id = 3
) LIMIT 1;

INSERT INTO `roles` (id, name, description) 
SELECT * FROM (SELECT 4, 'REPORTER', 'Can view only the reports and statistics') AS tmp
WHERE NOT EXISTS (
    SELECT id FROM roles WHERE id = 4
) LIMIT 1;

INSERT INTO `users` (id, username, password, isActive) VALUES (1, 'admin', '$2a$10$TJleQPJO2OKKAzJHkbROjuy/4h2ARF.3Dgc4XJrp2jtylqZZhPgbK', true);

INSERT INTO `users_roles` (user_id, role_id) VALUES (1, 1);
