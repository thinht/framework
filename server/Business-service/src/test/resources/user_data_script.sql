INSERT INTO `roles` (id, role) VALUES (1, 'ROLE_USER');
INSERT INTO `roles` (id, role) VALUES (2, 'ROLE_ADMIN');
INSERT INTO `roles` (id, role) VALUES (3, 'ROLE_SUPPER_ADMIN');
INSERT INTO `roles` (id, role) VALUES (4, 'ROLE_REPORTER');

INSERT INTO `users` (id, username, password) VALUES (1, 'admin', '$2a$10$TJleQPJO2OKKAzJHkbROjuy/4h2ARF.3Dgc4XJrp2jtylqZZhPgbK');

INSERT INTO `user_roles` (user_id, role_id) VALUES (1, 1);
INSERT INTO `user_roles` (user_id, role_id) VALUES (1, 2);
INSERT INTO `user_roles` (user_id, role_id) VALUES (1, 3);

INSERT INTO `accounts`
(firstname, lastname, pictureUri, phoneNumbers, websiteUrl, biography, isActive, user_id) VALUES
('Alan', 'Smith',  'myhouse.jpg', '0889907855',
'http://eztek.vn', 'Alan Smith (born 28 October 1980) is an English footballer who plays for Notts County', 1, 1);