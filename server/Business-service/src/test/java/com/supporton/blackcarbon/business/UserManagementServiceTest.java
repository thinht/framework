//package com.supporton.blackcarbon.business;
//
//import java.sql.SQLException;
//import java.util.List;
//
//import javax.persistence.EntityManager;
//
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.DataIntegrityViolationException;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.supporton.blackcarbon.business.base.BaseTest;
//import com.supporton.blackcarbon.business.dto.UserDTO;
//import com.supporton.blackcarbon.business.exceptions.DataValidationException;
//import com.supporton.blackcarbon.business.exceptions.NotFoundException;
//import com.supporton.blackcarbon.business.service.UserManagementService;
//import com.supporton.blackcarbon.business.utils.UserDTOTestDataBuilder;
//
///**
// * @author camthinh.tat
// * 
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//public class UserManagementServiceTest extends BaseTest {
//
//	@Autowired
//	private UserManagementService serviceUnderTest;
//
//	// @Autowired
//	// EntityManager entityManager;
//	//
//	// private UserDTO inputTestData, inputTestDataWithoutUsername,
//	// inputTestDataWithWrongEmail,
//	// inputTestDataDuplicateUsername;
//	//
//	// private static final String SQL_USER_CRIPTS = "user_data_script.sql";
//	// private static final String SQL_CLEAN_DATA_SCRIPT =
//	// "clean_data_script.sql";
//
//	// @Before
//	// public void setUp() {
//	// executeSqlScript(SQL_USER_CRIPTS, false);
//	//
//	// Assert.assertNotNull("userManagementService must be not null",
//	// serviceUnderTest);
//	//
//	// // Setting up test data
//	// inputTestData =
//	// new
//	// UserDTOTestDataBuilder().username("thinhtat123").firstName("Thinh").lastName("Tat")
//	// .email("thinh.tat@eztek.vn").role(1L, "ROLE_ADMIN").build();
//	//
//	// inputTestDataDuplicateUsername =
//	// new
//	// UserDTOTestDataBuilder().username("thinhtat").firstName("Thinh").lastName("Tat")
//	// .email("thinh.tat@eztek.vn").build();
//	//
//	// inputTestDataWithoutUsername =
//	// new
//	// UserDTOTestDataBuilder().firstName("Thinh").lastName("Tat").email("thinh.tat@eztek.vn")
//	// .build();
//	//
//	// // data test email
//	// inputTestDataWithWrongEmail =
//	// new
//	// UserDTOTestDataBuilder().firstName("Thinh").lastName("Tat").email("thinh.tat.vn")
//	// .build();
//	// }
//	//
//	// @After
//	// public void tearDown() throws SQLException {
//	// executeSqlScript(SQL_CLEAN_DATA_SCRIPT, false);
//	// }
//	//
//	// @Test
//	// public void testCreateAccount() throws DataValidationException {
//	// UserDTO actual = serviceUnderTest.createAccount(inputTestData);
//	//
//	// Assert.assertNotNull("User return from insertion must be not null",
//	// actual);
//	// Assert.assertTrue("Should has id after insert", actual.getId() > 0);
//	// Assert.assertEquals("Username should be equal",
//	// inputTestData.getUsername(),
//	// actual.getUsername());
//	// Assert.assertEquals("First name should be equal", actual.getFirstName(),
//	// inputTestData.getFirstName());
//	// Assert.assertEquals("Last name should be equal", actual.getLastName(),
//	// inputTestData.getLastName());
//	// }
//	//
//	// @Test(expected = DataIntegrityViolationException.class)
//	// public void testCreateAccountWithDuplicatedUsername() throws
//	// DataValidationException {
//	// serviceUnderTest.createAccount(inputTestDataDuplicateUsername);
//	// }
//	//
//	// @Test(expected = DataValidationException.class)
//	// public void testCreateAccountWithUsernameIsEmpty() throws
//	// DataValidationException {
//	// serviceUnderTest.createAccount(inputTestDataWithoutUsername);
//	// }
//	//
//	// @Test(expected = DataValidationException.class)
//	// public void testCreateAccountWithWrongEmail() throws
//	// DataValidationException {
//	// serviceUnderTest.createAccount(inputTestDataWithWrongEmail);
//	// }
//	//
//	// @Test(expected = NotFoundException.class)
//	// public void testFindAccountByIdWithNotFoundException() throws
//	// NotFoundException {
//	// Long id = 999L;
//	// serviceUnderTest.findAccountById(id);
//	// }
//	//
//	// @Test
//	// public void testFindAccountById() throws NotFoundException {
//	// Long expectedId = 1L;
//	// String expectedUsername = "alansmith";
//	// UserDTO userDto = serviceUnderTest.findAccountById(expectedId);
//	//
//	// Assert.assertNotNull("User must be not null", userDto);
//	// Assert.assertEquals("Username should be equal", userDto.getUsername(),
//	// expectedUsername);
//	// }
//
//	// @Test
//	// public void testGetAllAccountPagination() throws NotFoundException {
//	// long expectedNumOfUsers = 6;
//	// int pageNumber = 1;
//	//
//	// List<UserDTO> users =
//	// serviceUnderTest.getAllAccountPagination(pageNumber);
//	// for (UserDTO userDto : users) {
//	// System.out.println(userDto.getUsername());
//	// }
//	//
//	// Assert.assertTrue("User list should have some records", users.size() > 0
//	// && users.size() == expectedNumOfUsers);
//	// }
//
//	@Test
//	public void testFindAccountByUsername() throws NotFoundException {
//		String username = "dangpham";
//
//		UserDTO user = serviceUnderTest.findAccountByUsername(username);
//		Assert.assertNotNull("User must be not null", user);
//		//Assert.assertTrue("User Id must exist", user.getId() == 1);
//		System.out.println("user name " + user.getUsername());
//	}
//
//	@Test
//	public void testFindAlls() throws NotFoundException {
//		List<UserDTO> users = serviceUnderTest.getAllAccounts();
//		for (UserDTO userDto : users) {
//			System.out.println(userDto.getUsername());
//		}
//
//	}
//	// @Test(expected = NotFoundException.class)
//	// public void testFindAccountByUsernameWithNotFoundException() throws
//	// NotFoundException {
//	// String username = "notfoundthisuser";
//	//
//	// serviceUnderTest.findAccountByUsername(username);
//	// }
//
//	// @Test
//	// public void testFindAccountByEmail() throws NotFoundException {
//	// String email = "alansmith@gmail.com";
//	// Long expectedUserId = 1L;
//	// String expectedUsername = "alansmith";
//	//
//	// UserDTO user = serviceUnderTest.findAccountByEmail(email);
//	// Assert.assertNotNull("User must be not null", user);
//	// Assert.assertTrue("User Id must be correct", user.getId() ==
//	// expectedUserId);
//	// Assert.assertTrue("Username must be correct",
//	// user.getUsername().equals(expectedUsername));
//	// }
//	//
//	// @Test
//	// public void testForceDeleteAccount() throws NotFoundException {
//	// Long expectedUserId = 1L;
//	//
//	// serviceUnderTest.forceDeleteAccount(expectedUserId);
//	// }
//}
