//package com.supporton.blackcarbon.business;
//
//import java.sql.SQLException;
//import java.util.Properties;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.supporton.blackcarbon.business.base.BaseTest;
//import com.supporton.blackcarbon.business.service.EmailService;
//
///**
// * @author camthinh.tat
// * 
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//public class EmailServiceTest extends BaseTest {
//
//  @Autowired
//  private EmailService emailService;
//
//  @Before
//  public void setUp() {}
//
//  @After
//  public void tearDown() throws SQLException {}
//
//  @Test
//  public void testSendMail() {
//    final String username = "thinh.tat84@gmail.com";
//    final String password = "p@ssword123!";
//
//    Properties props = new Properties();
//    props.put("mail.smtp.auth", "true");
//    props.put("mail.smtp.starttls.enable", "true");
//    props.put("mail.smtp.host", "smtp.gmail.com");
//    props.put("mail.smtp.port", "587");
//    props.put("mail.debug", "true");
//    props.put("username", username);
//    props.put("password", password);
//    
//    emailService.sendRegistrationEmail(props, "emailTemplate.vm", "Thinh","Tat","/test");
//  }
//}
