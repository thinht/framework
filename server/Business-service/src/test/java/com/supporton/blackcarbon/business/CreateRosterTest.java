//package com.supporton.blackcarbon.business;
//
//import java.io.IOException;
//import java.sql.SQLException;
//import java.util.Collection;
//
//import javax.security.sasl.SaslException;
//
//import org.jivesoftware.smack.PacketListener;
//import org.jivesoftware.smack.Roster;
//import org.jivesoftware.smack.RosterEntry;
//import org.jivesoftware.smack.SmackException;
//import org.jivesoftware.smack.SmackException.NotConnectedException;
//import org.jivesoftware.smack.XMPPConnection;
//import org.jivesoftware.smack.XMPPException;
//import org.jivesoftware.smack.filter.PacketFilter;
//import org.jivesoftware.smack.packet.Packet;
//import org.jivesoftware.smack.packet.Presence;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.supporton.blackcarbon.business.base.BaseTest;
//import com.supporton.blackcarbon.business.dto.CustomerDTO;
//import com.supporton.blackcarbon.business.service.ChatService;
//import com.supporton.blackcarbon.business.service.CustomerService;
//import com.supporton.blackcarbon.model.Customer;
//import com.supporton.blackcarbon.model.User;
//import com.supporton.blackcarbon.repository.UserRepository;
//
///**
// * @author camthinh.tat
// *
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//public class CreateRosterTest extends BaseTest {
//
//  @Autowired
//  private CustomerService customerService;
//
//  @Autowired
//  private UserRepository userRepository;
//
//  @Autowired
//  private ChatService chatService;
//
//  @Before
//  public void setUp() {}
//
//  @After
//  public void tearDown() throws SQLException {}
//
////  @Test
////  public void testCreateGroupEntriesForAgent() throws SaslException, SmackException, IOException,
////      XMPPException, Exception {
////    Customer customer = customerService.findCustomerByEmail("thinh.tat8884@gmail.com");
////    User user = userRepository.findOne(1L);
////
////    chatService.connect();
////    chatService.login("thinhtat3", "123123");
////
////    Roster roster = chatService.getConnection().getRoster();
////    roster.setSubscriptionMode(SubscriptionMode.accept_all);
////    roster.createEntry(customer.getName() + "@eztek-window", customer.getName(), null);
////
////    System.out.println("done");
////    chatService.getConnection().disconnect();
////  }
//
//  @Test
//  public void sendSubcriptionToAgent() throws SaslException, SmackException, IOException,
//      XMPPException, Exception {
//    chatService.connect();
//    final XMPPConnection conn = chatService.getConnection();
//    chatService.login("thinhtat333", "123123");
//
//    Presence subscribe = new Presence(Presence.Type.subscribe);
//    subscribe.setTo("thinhtat3@eztek-window");
//    conn.sendPacket(subscribe);
//
//    conn.addPacketListener(new PacketListener() {
//
//      @Override
//      public void processPacket(Packet packet) throws NotConnectedException {
//        if (packet instanceof Presence) {
//          Presence presence = (Presence) packet;
//
//          String email = presence.getFrom();
//          System.out.println("chat invite status changed by user: : " + email + " calling listner");
//          System.out.println("presence: " + presence.getFrom() + "; type: " + presence.getType()
//              + "; to: " + presence.getTo() + "; " + presence.toXML());
//          Roster roster = conn.getRoster();
//          for (RosterEntry rosterEntry : roster.getEntries()) {
//            System.out.println("jid: " + rosterEntry.getUser() + "; type: " + rosterEntry.getType()
//                + "; status: " + rosterEntry.getStatus());
//          }
//          System.out.println("\n\n\n");
//          if (presence.getType().equals(Presence.Type.subscribe)) {
//            Presence newp = new Presence(Presence.Type.subscribed);
//            newp.setMode(Presence.Mode.available);
//            newp.setPriority(24);
//            newp.setTo(presence.getFrom());
//            conn.sendPacket(newp);
//            Presence subscription = new Presence(Presence.Type.subscribe);
//            subscription.setTo(presence.getFrom());
//            conn.sendPacket(subscription);
//
//          } else if (presence.getType().equals(Presence.Type.unsubscribe)) {
//            Presence newp = new Presence(Presence.Type.unsubscribed);
//            newp.setMode(Presence.Mode.available);
//            newp.setPriority(24);
//            newp.setTo(presence.getFrom());
//            conn.sendPacket(newp);
//          }
//        }
//      }
//    }, new PacketFilter() {
//
//      @Override
//      public boolean accept(Packet packet) {
//        if (packet instanceof Presence) {
//          Presence presence = (Presence) packet;
//          if (presence.getType().equals(Presence.Type.subscribed)
//              || presence.getType().equals(Presence.Type.subscribe)
//              || presence.getType().equals(Presence.Type.unsubscribed)
//              || presence.getType().equals(Presence.Type.unsubscribe)) {
//            return true;
//          }
//        }
//        return false;
//      }
//    });
//
//    Thread.sleep(50000);
//    System.out.println("done");
//    chatService.getConnection().disconnect();
//  }
//
//  @Test
//  public void listAllRosterEntriesAgent() throws SaslException, SmackException, IOException,
//      XMPPException, Exception {
//    CustomerDTO customer = customerService.findCustomerByEmail("thinh.tat8884@gmail.com");
//    User user = userRepository.findOne(1L);
//
//    chatService.connect();
//    chatService.login("thinhtat3", "123123");
//
//    Roster roster = chatService.getConnection().getRoster();
//    Collection<RosterEntry> entries = roster.getEntries();
//    for (RosterEntry entry : entries) {
//      System.out.println(entry);
//    }
//    chatService.getConnection().disconnect();
//    System.out.println("done");
//  }
//
//  @Test
//  public void listAllRosterEntriesCustomer() throws SaslException, SmackException, IOException,
//      XMPPException, Exception {
//    chatService.connect();
//    chatService.login("thinhtat3", "123123");
//
//    Roster roster = chatService.getConnection().getRoster();
//    Collection<RosterEntry> entries = roster.getEntries();
//    for (RosterEntry entry : entries) {
//      System.out.println(entry);
//    }
//    chatService.getConnection().disconnect();
//    System.out.println("done");
//  }
//
//  @Test
//  public void deleteAllRosterEntries() throws SaslException, SmackException, IOException,
//      XMPPException, Exception {
//    CustomerDTO customer = customerService.findCustomerByEmail("thinh.tat8884@gmail.com");
//    User user = userRepository.findOne(1L);
//
//    chatService.connect();
//    chatService.login("thinhtat3", "123123");
//
//    Roster roster = chatService.getConnection().getRoster();
//    Collection<RosterEntry> entries = roster.getEntries();
//    for (RosterEntry entry : entries) {
//      roster.removeEntry(entry);
//    }
//    chatService.getConnection().disconnect();
//    // Thread.sleep(10000);
//    System.out.println("done");
//  }
//
//  @Test
//  public void checkAvailableUser() throws SaslException, SmackException, IOException,
//      XMPPException, Exception {
//    XMPPConnection connection = chatService.connect();
//    chatService.login("thinhtat3", "123123");
//    Presence newp = new Presence(Presence.Type.available);
//    newp.setMode(Presence.Mode.available);
//    connection.sendPacket(newp);
//
//    XMPPConnection connection2 = chatService.connect();
//    chatService.login("thinhtat333", "123123");
//    Roster roster = connection2.getRoster();
//    Presence availability = roster.getPresence("thinhtat3@eztek-window");
//
//    chatService.getConnection().disconnect();
//  }
//}
