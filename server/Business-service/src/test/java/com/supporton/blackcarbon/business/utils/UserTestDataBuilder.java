package com.supporton.blackcarbon.business.utils;

import com.supporton.blackcarbon.model.User;

/**
 * @author thinhtat
 * 
 */
public class UserTestDataBuilder {
  private User user;

  public UserTestDataBuilder() {
    user = new User();
  }

  public User build() {
    return user;
  }

  public UserTestDataBuilder id(Long id) {
    user.setId(id);
    return this;
  }

  public UserTestDataBuilder username(String username) {
    user.setUsername(username);
    return this;
  }

  public UserTestDataBuilder password(String password) {
    user.setPassword(password);
    return this;
  }
}
