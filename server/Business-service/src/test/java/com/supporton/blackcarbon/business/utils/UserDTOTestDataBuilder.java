package com.supporton.blackcarbon.business.utils;

import com.supporton.blackcarbon.business.dto.RoleDTO;
import com.supporton.blackcarbon.business.dto.UserDTO;

public class UserDTOTestDataBuilder {
  private UserDTO userDTO;

  public UserDTOTestDataBuilder() {
    userDTO = new UserDTO();
  }

  public UserDTO build() {
    return userDTO;
  }

  public UserDTOTestDataBuilder username(String username) {
    userDTO.setUsername(username);
    return this;
  }

  public UserDTOTestDataBuilder firstName(String firstName) {
    userDTO.setFirstName(firstName);
    return this;
  }

  public UserDTOTestDataBuilder lastName(String lastName) {
    userDTO.setLastName(lastName);
    return this;
  }

  

  public UserDTOTestDataBuilder role(long id, String name) {
    RoleDTO role1 = new RoleDTO();
    role1.setId(id);
    role1.setRole(name);
    userDTO.getRoles().add(role1);
    return this;
  }
}
