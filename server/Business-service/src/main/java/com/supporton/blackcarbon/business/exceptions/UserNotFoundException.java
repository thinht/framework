package com.supporton.blackcarbon.business.exceptions;

/**
 * @author justinchasez
 *
 */
public class UserNotFoundException extends Exception {
  private static final long serialVersionUID = 1L;
}
