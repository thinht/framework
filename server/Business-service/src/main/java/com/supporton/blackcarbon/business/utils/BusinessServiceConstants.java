package com.supporton.blackcarbon.business.utils;

/**
 * @author justinchasez
 *
 */
public class BusinessServiceConstants {
  public static final int DEFAULT_PAGE_SIZE = 20;
  public static final String DEEFAULT_SORT_BY = "id";
  public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
      + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
  public static final String RESULT_SUCCESS = "success";
  public static final String RESULT_ERROR = "error";

  public static final String RESULT_ERROR_USER_NOT_FOUND = "User not found.";
  public static final String RESULT_ERROR_INPUT_VALIDATION = "Input data is not correct.";
  public static final String RESULT_ERROR_INTERNAL_SERVER_ERROR = "Internal server error";
}
