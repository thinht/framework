package com.supporton.blackcarbon.business.mappers;

import org.modelmapper.PropertyMap;

import com.supporton.blackcarbon.business.dto.RoleDTO;
import com.supporton.blackcarbon.model.Role;

/**
 * @author camthinh.tat
 * 
 */
public class RoleFromDTO extends PropertyMap<RoleDTO, Role> {

	@Override
	protected void configure() {
		map().setId(source.getId());
		map().setRole(source.getRole());
	}
}
