package com.supporton.blackcarbon.business.exceptions;

/**
 * @author justinchasez
 *
 */
public class AlreadyExistException extends Exception {
  private static final long serialVersionUID = 1L;
}
