package com.supporton.blackcarbon.business.mappers;

import org.modelmapper.PropertyMap;

import com.supporton.blackcarbon.business.dto.UserDTO;
import com.supporton.blackcarbon.model.User;

/**
 * @author camthinh.tat
 * 
 */
public class UserDTOToUserMapper extends PropertyMap<UserDTO, User> {

	@Override
	protected void configure() {
		skip().setId(null);
		map().setMiddlename(source.getMiddleName());
		map().setNickname(source.getNickName());
		map().setBiography(source.getBiography());
		map().setFirstname(source.getFirstName());
		map().setLastname(source.getLastName());
		map().setPhoneNumbers(source.getPhoneNumbers());
		map().setPictureUri(source.getPictureUri());
		map().setUsername(source.getUsername());
		map().setEmail(source.getUsername());
		skip().setRole(null);
		skip().setPassword(null);
		map().setWebsiteUrl(source.getWebsiteUrl());
	}
}
