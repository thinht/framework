package com.supporton.blackcarbon.business.service;

import java.util.List;

import com.supporton.blackcarbon.business.dto.RoleDTO;
import com.supporton.blackcarbon.business.exceptions.DataValidationException;
import com.supporton.blackcarbon.business.exceptions.NotFoundException;

public interface RolesManagementService {
  
	RoleDTO createRole(RoleDTO roleDTO) throws DataValidationException;

	List<RoleDTO> findRolesById(Long id) throws NotFoundException;

	void deleteRole(Long id) throws NotFoundException;

	RoleDTO updateRole(Long id, RoleDTO roleDTO) throws NotFoundException, DataValidationException;

}
