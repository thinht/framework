package com.supporton.blackcarbon.business.service.impl;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.supporton.blackcarbon.business.dto.UserDTO;
import com.supporton.blackcarbon.business.exceptions.DataValidationException;
import com.supporton.blackcarbon.business.exceptions.NotFoundException;
import com.supporton.blackcarbon.business.mappers.UserDTOToUserMapper;
import com.supporton.blackcarbon.business.mappers.UserToUserDTOMapper;
import com.supporton.blackcarbon.business.service.UserManagementService;
import com.supporton.blackcarbon.model.User;
import com.supporton.blackcarbon.repository.RoleRepository;
import com.supporton.blackcarbon.repository.UserRepository;

/**
 * @author camthinh.tat
 * 
 */
@Service
public class UserManagementServiceImpl implements UserManagementService {

	private static final Logger logger = LoggerFactory.getLogger(UserManagementServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private Validator validator;

	private ModelMapper mapper;

	public UserManagementServiceImpl() {
		mapper = new ModelMapper();

		// validation passed, define mapping
		mapper.addMappings(new UserDTOToUserMapper());
		mapper.addMappings(new UserToUserDTOMapper());
	}

	private void validate(UserDTO userDTO) throws DataValidationException {

		Set<ConstraintViolation<UserDTO>> constraintViolations = validator.validate(userDTO);

		for (ConstraintViolation<UserDTO> contraint : constraintViolations) {
			logger.info(contraint.getPropertyPath() + " " + contraint.getMessage());
		}

		if (constraintViolations.size() > 0) {
			throw new DataValidationException("Missing information");
		}
	}

	@Override
	public UserDTO authenticate(String username, String password) throws NotFoundException {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String errorMessage = "Invalid username and password, please try again!";
		UserDTO userDTO = new UserDTO();
		User user = userRepository.findByUsername(username);
		CharSequence s = password;

		if (user != null && passwordEncoder.matches(s, user.getPassword()) && user.getIsActive().booleanValue()) {
			userDTO = mapper.map(user, UserDTO.class);
		}else{
			throw new NotFoundException(errorMessage);
		}

		return userDTO;
	}

	@Override
	public UserDTO findUserByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void verifyToken(String token) {
		// TODO Auto-generated method stub

	}

}
