package com.supporton.blackcarbon.business.dto;

public class RoleDTO {
  private Long id;

  private String role;
  
  private String description;

  public RoleDTO() {}

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }
}
