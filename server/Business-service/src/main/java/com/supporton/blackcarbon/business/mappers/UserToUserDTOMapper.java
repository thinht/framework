package com.supporton.blackcarbon.business.mappers;

import org.modelmapper.PropertyMap;

import com.supporton.blackcarbon.business.dto.UserDTO;
import com.supporton.blackcarbon.model.User;

/**
 * @author camthinh.tat
 * 
 */
public class UserToUserDTOMapper extends PropertyMap<User, UserDTO> {
	@Override
	protected void configure() {
		map().setId(source.getId());
		map().setBiography(source.getBiography());
		map().setFirstName(source.getFirstname());
		map().setLastName(source.getLastname());
		map().setMiddleName(source.getMiddlename());
		map().setNickName(source.getNickname());
		map().setPhoneNumbers(source.getPhoneNumbers());
		map().setPictureUri(source.getPictureUri());
		skip().setRoles(null);
		map().setUsername(source.getUsername());
		skip().setPassword(null);
		map().setEmail(source.getUsername());
		map().setWebsiteUrl(source.getWebsiteUrl());
	}
}
