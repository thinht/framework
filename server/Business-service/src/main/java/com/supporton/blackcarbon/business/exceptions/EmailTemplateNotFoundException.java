package com.supporton.blackcarbon.business.exceptions;

public class EmailTemplateNotFoundException extends Exception {
  private static final long serialVersionUID = 1L;
}
