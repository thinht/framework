package com.supporton.blackcarbon.business.service;

import com.supporton.blackcarbon.business.dto.UserDTO;
import com.supporton.blackcarbon.business.exceptions.NotFoundException;

/**
 * @author camthinh.tat
 * 
 */
public interface UserManagementService {
	UserDTO authenticate(String account, String password) throws NotFoundException;
	
	UserDTO findUserByUsername(String username);
	
	void verifyToken(String token);
}
