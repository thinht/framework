package com.supporton.blackcarbon.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supporton.blackcarbon.business.dto.RoleDTO;
import com.supporton.blackcarbon.business.exceptions.DataValidationException;
import com.supporton.blackcarbon.business.exceptions.NotFoundException;
import com.supporton.blackcarbon.business.service.RolesManagementService;
import com.supporton.blackcarbon.repository.RoleRepository;

/**
 * @author thinhtat
 * 
 */
@Service
public class RoleManagementServiceImpl implements RolesManagementService {

	@Autowired
	RoleRepository roleRepository;

	@Override
	public RoleDTO createRole(RoleDTO roleDTO) throws DataValidationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RoleDTO> findRolesById(Long id) throws NotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteRole(Long id) throws NotFoundException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public RoleDTO updateRole(Long id, RoleDTO roleDTO) throws NotFoundException, DataValidationException {
		// TODO Auto-generated method stub
		return null;
	}

}
