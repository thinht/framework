#SupportOn - Web API

**What do we use:
	- Java 7
	- Spring Framework Core
	- Spring Security Oauth 2
	- Spring Data JPA
	- Jersey Restful Web service
	- Object Mapper
	- MySQL
	- Maven

**Setting up Environment Variables:
	Maven
		M2_HOME=path to Maven folder
		JAVA_HOME=path to jdk folder
		PATH=%JAVA_HOME%\bin;%M2_HOME%\bin

**How to run this project:

	1 - Import SQL data
		Config database at: /supportOnServer/WebApis/src/main/resources/data-source.properties

	2 - Open Terminal at root project: /supportOnServer
		run: mvn install

	3 - To WebApiV2 project /supportOnServer/WebApis
		run mvn jetty:run

** API Design:

		Authentication
			Access Token: 
				Api		 : http://localhost:8080/webapi/oauth/token
				data	 : {
								grant_type=password,
								client_id=clientId,
								client_secret=secret,
								username={username},
								password={password},
							}
					{username}: Base64[YWRtaW4] = admin
					{password}: Base64[MTIzMTIz] = 123123
			
			Refresh Token:
				Api		: http://localhost:8080/webapi/oauth/token
				data	: {
								grant_type=refresh_token
								client_id=clientId
								client_secret=secret
								refresh_token=[refresh token from /oauth/token]	
							}
							
			LogOut:
				Api		: http://localhost:8080/webapi/oauth/logout
				Header	: Authorization: Bearer [tokenkey]
							