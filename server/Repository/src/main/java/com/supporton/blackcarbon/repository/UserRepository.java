package com.supporton.blackcarbon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.supporton.blackcarbon.model.User;

/**
 * @author thinhtat
 * 
 */
public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);

	List<User> findByEmail(String email);
}
