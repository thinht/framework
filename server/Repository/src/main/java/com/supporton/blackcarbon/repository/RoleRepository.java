package com.supporton.blackcarbon.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.supporton.blackcarbon.model.Role;

/**
 * @author thinhtat
 * 
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
  Role findByRole(String roleName);
}
