//package com.supporton.blackcarbon.repository;
//
//import java.sql.SQLException;
//
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.DataIntegrityViolationException;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.supporton.blackcarbon.model.User;
//import com.supporton.blackcarbon.repository.base.BaseTest;
//import com.supporton.blackcarbon.repository.testdata.TestData;
//
///**
// * @author camthinh.tat
// * 
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//public class UserRepositoryTest extends BaseTest {
//
//	@Autowired
//	private UserRepository userRepositoryUnderTest;
//
//	private static final String SQL_USER_CRIPTS = "user_data_script.sql";
//	private static final String SQL_CLEAN_DATA_SCRIPT = "clean_data_script.sql";
//
//	@Before
//	public void setUp() {
//		 Assert.assertNotNull("UserRepository must be not null", userRepositoryUnderTest);
//		 executeSqlScript(SQL_USER_CRIPTS, false);
//
//	}
//
//	//
//	// @After
//	// public void tearDown() throws SQLException {
//	// executeSqlScript(SQL_CLEAN_DATA_SCRIPT, false);
//	// }
//
//
//	//
//	// @Test(expected = DataIntegrityViolationException.class)
//	// public void testInsertProfileWithoutEmail() {
//	// User user = TestData.createUserWithoutEmail();
//	// userRepositoryUnderTest.save(user);
//	// }
//	//
////	@Test
////	public void testInsertUser() {
////		User user = TestData.createUserForTest();
////		//User user1 = TestData.createUserForTestDuplicate();
////		User addedUser = userRepositoryUnderTest.save(user);
////		//User addedUser1 = userRepositoryUnderTest.save(user1);
////		Assert.assertNotNull("must be not null", addedUser);
////		Assert.assertTrue("must be not null", addedUser.getId() > 0);
////		Assert.assertEquals("must be equal", addedUser.getRole().size(), 1);
////	}
//
////	private void testInsertUser() {
////		User user = TestData.createUserForTest();
////		// User user1 = TestData.createUserForTestDuplicate();
////		User addedUser = userRepositoryUnderTest.save(user);
////		// User addedUser1 = userRepositoryUnderTest.save(user1);
////		Assert.assertNotNull("must be not null", addedUser);
////		Assert.assertTrue("must be not null", addedUser.getId() > 0);
////		Assert.assertEquals("must be equal", addedUser.getRole().size(), 1);
////	}
//	
//	@Test
//	public void testFindByUsername() {
//		//testInsertUser();
//		
//		User realUser = userRepositoryUnderTest.findByUsername("admin");
//
//		// can not load lazy here because session is closed.
//		// TODO: set @Transactional on @Service layer
//		Assert.assertNotNull("Retrieved user must be not null", realUser);
//		Assert.assertEquals("Retrieved must be contains 2 roles", realUser.getRole().size(), 1);
//	}
//	//
//	// /*
//	// * @Test public void testHashedPassword() { User user =
//	// TestData.createUserForTest(); User
//	// * addedUser = userRepositoryUnderTest.save(user);
//	// Assert.assertNotNull("must be not null",
//	// * addedUser); logger.info(user.getPassword() + " / " +
//	// addedUser.getPasswordSalt());
//	// * Assert.assertTrue("Password should be equal to pa	sswordSalt",
//	// * addedUser.getPasswordSalt().equals(user.getPassword()));
//	// * Assert.assertEquals("must be not equal",
//	// !addedUser.getPassword().equals(user.getPassword()));
//	// * }
//	// */
//	//
//	// @Test(expected = DataIntegrityViolationException.class)
//	// public void testInsertDuplicateUsername() {
//	// User user = TestData.createUserForTestDuplicate();
//	// userRepositoryUnderTest.save(user);
//	// }
//	//
//	// @Test(expected = DataIntegrityViolationException.class)
//	// public void testInsertDuplicateEmail() {
//	// User user = TestData.createUserForTestDuplicate();
//	// userRepositoryUnderTest.save(user);
//	// }
//}
