DROP TABLE IF EXISTS `supporton`.`users`;
CREATE TABLE  `supporton`.`users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isActivate` bit(1) NOT NULL,
  `confirmationToken` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordSalt` varchar(255) DEFAULT NULL,
  `passwordVerificationToken` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_r43af9ap4edm43mmtq01oddj6` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supporton`.`user_profile`;
CREATE TABLE  `supporton`.`user_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdDate` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `isMarkedAsDeleted` bit(1) NOT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `modifiedUser` varchar(255) DEFAULT NULL,
  `biography` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `isActive` int(11) DEFAULT NULL,
  `lastname` varchar(255) NOT NULL,
  `phoneNumbers` varchar(255) DEFAULT NULL,
  `pictureUri` varchar(255) DEFAULT NULL,
  `websiteUrl` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ebc21hy5j7scdvcjt0jy6xxrv` (`user_id`),
  CONSTRAINT `FK_ebc21hy5j7scdvcjt0jy6xxrv` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supporton`.`roles`;
CREATE TABLE  `supporton`.`roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdDate` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `isActivate` bit(1) NOT NULL,
  `isMarkAsDeleted` bit(1) NOT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `modifiedUser` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supporton`.`user_roles`;
CREATE TABLE  `supporton`.`user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK_5q4rc4fh1on6567qk69uesvyf` (`role_id`),
  CONSTRAINT `FK_g1uebn6mqk9qiaw45vnacmyo2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_5q4rc4fh1on6567qk69uesvyf` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `supporton`.`modules`;
CREATE TABLE  `supporton`.`modules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supporton`.`accounts`;
CREATE TABLE  `supporton`.`accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdDate` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `isActivate` bit(1) NOT NULL,
  `isMarkAsDeleted` bit(1) NOT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `modifiedUser` varchar(255) DEFAULT NULL,
  `accountType` varchar(255) DEFAULT NULL,
  `activationDate` datetime DEFAULT NULL,
  `biography` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `isActive` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_e4w4av1wrhanry7t6mxt42nou` (`user_id`),
  CONSTRAINT `FK_e4w4av1wrhanry7t6mxt42nou` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supporton`.`accounts_modules`;
CREATE TABLE  `supporton`.`accounts_modules` (
  `accounts_id` bigint(20) NOT NULL,
  `module_id` bigint(20) NOT NULL,
  PRIMARY KEY (`accounts_id`,`module_id`),
  KEY `FK_kug4yjf1m4sqk3ku6edv6kx6c` (`module_id`),
  CONSTRAINT `FK_pdvlojmqksd20513hh1jea4` FOREIGN KEY (`accounts_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `FK_kug4yjf1m4sqk3ku6edv6kx6c` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supporton`.`departments`;
CREATE TABLE  `supporton`.`departments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdDate` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `isActivate` bit(1) NOT NULL,
  `isMarkAsDeleted` bit(1) NOT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `modifiedUser` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_3mwpruvtvrqlx6d8alrwe16oh` (`account_id`),
  CONSTRAINT `FK_3mwpruvtvrqlx6d8alrwe16oh` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `supporton`.`user_accounts_departments`;
CREATE TABLE  `supporton`.`user_accounts_departments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `accounts_id` bigint(20) NOT NULL,
  `department_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_dhkfkgkredz34567xx1edf7` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_pdvlojmqksd20513hh1jea3` FOREIGN KEY (`accounts_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `FK_kug4yjf1m4sqk3ku6edv6kx7c` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;