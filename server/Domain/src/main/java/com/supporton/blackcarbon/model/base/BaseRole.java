package com.supporton.blackcarbon.model.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class BaseRole extends BaseDomain {
	@Transient
	private static final long serialVersionUID = 1L;

	@Column(unique = true)
	private String name;
	private String description;

	public BaseRole() {
	}

	public BaseRole(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
