package com.supporton.blackcarbon.model.auth;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import com.supporton.blackcarbon.model.base.BaseUser;

@Entity
@Table(name = "users")
@Immutable
public class UserAuthentication extends BaseUser{
	private static final long serialVersionUID = 6744212833736753340L;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "users_roles", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false, updatable = false) })
	private Set<Role> role;

	public UserAuthentication() {
	}
	
	public Set<Role> getRole() {
		return role;
	}

	public void setRole(Set<Role> role) {
		this.role = role;
	}
	
}
