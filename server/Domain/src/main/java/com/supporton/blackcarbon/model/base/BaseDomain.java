package com.supporton.blackcarbon.model.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * @author thinhtat
 * 
 */
@MappedSuperclass
public abstract class BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private Date createdDate;
	private String createdUser;
	private Date modifiedDate;
	private String modifiedUser;
	private Boolean isActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(String modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@PrePersist
	void persist() {
		if (this.isActive == null) {
			this.isActive = Boolean.FALSE;
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof OAuth2Authentication) {
			OAuth2Authentication ouAuth2Authentication = (OAuth2Authentication) authentication;
			if (authentication != null && authentication.getPrincipal() != null) {
				this.setCreatedUser(ouAuth2Authentication.getUserAuthentication().getPrincipal().toString());
			} else {
				this.setCreatedUser(authentication.getPrincipal().toString());
			}
		} else {
			this.setCreatedUser("");
		}
		this.createdDate = new Date();
	}

	@PreUpdate()
	void preUpdate() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof OAuth2Authentication) {
			OAuth2Authentication ouAuth2Authentication = (OAuth2Authentication) authentication;
			if (authentication != null && authentication.getPrincipal() != null) {
				this.setModifiedUser(ouAuth2Authentication.getUserAuthentication().getPrincipal().toString());
			} else {
				this.setModifiedUser(authentication.getPrincipal().toString());
			}
		} else {
			this.setModifiedUser("");
		}

		this.modifiedDate = new Date();
	}
}
