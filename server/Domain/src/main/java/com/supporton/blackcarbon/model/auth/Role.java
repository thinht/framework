package com.supporton.blackcarbon.model.auth;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.supporton.blackcarbon.model.base.BaseRole;

/**
 * @author camthinh.tat
 * 
 */
@Entity
@Table(name = "roles")
public class Role extends BaseRole {

	@Transient
	private static final long serialVersionUID = 4004012691422765644L;
	
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "role")
	private List<UserAuthentication> userList;

	public Role() {
	}

	public List<UserAuthentication> getUserList() {
		return userList;
	}

	public void setUserList(List<UserAuthentication> userList) {
		this.userList = userList;
	}

}
