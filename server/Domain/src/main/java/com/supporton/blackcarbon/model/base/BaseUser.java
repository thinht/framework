package com.supporton.blackcarbon.model.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public class BaseUser extends BaseDomain {
	@Transient
	private static final long serialVersionUID = 1L;

	@Column(unique = true)
	@NotNull
	private String username;
	@NotNull
	private String password;
	
	public BaseUser() {
	}

	public BaseUser(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
