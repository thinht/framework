//package com.supporton.blackcarbon.repository.testdata;
//
//import java.util.HashSet;
//import java.util.Set;
//
//import com.supporton.blackcarbon.model.Account;
//import com.supporton.blackcarbon.model.Role;
//import com.supporton.blackcarbon.model.User;
//
//public class TestData {
//
//	public static User createUserForTest() {
//		User user = new User("dangpham", "123123");
//		Role role1 = new Role();
//		role1.setRole("ROLE_USER");
////		Role role2 = new Role();
////		role2.setRole("ROLE_ADMIN");
//		Set<Role> roles = new HashSet<Role>();
//		roles.add(role1);
////		roles.add(role2);
//		user.setRole(roles);
//
//		// profile
//		Account account = new Account();
//		account.setFirstname("Thinh");
//		account.setLastname("Cam Tat");
//		account.setUser(user);
//		user.setAccount(account);
//		return user;
//	}
//
//	public static User createUserForTestDuplicate() {
//		User user = new User("thinhtat", "123123");
//		Role role1 = new Role();
//		role1.setRole("ROLE_USER");
//		Role role2 = new Role();
//		role2.setRole("ROLE_ADMIN");
//		Set<Role> roles = new HashSet<Role>();
//		roles.add(role1);
//		roles.add(role2);
//		user.setRole(roles);
//
//		// profile
//		Account account = new Account();
//		account.setFirstname("Thinh");
//		account.setLastname("Cam Tat");
//		account.setUser(user);
//		user.setAccount(account);
//		return user;
//	}
//
//	public static User createUserForTestDuplicateEmail() {
//		User user = new User("thinhtat", "123123");
//		Role role1 = new Role();
//		role1.setRole("ROLE_USER");
//		Role role2 = new Role();
//		role2.setRole("ROLE_ADMIN");
//		Set<Role> roles = new HashSet<Role>();
//		roles.add(role1);
//		roles.add(role2);
//		// user.setRole(roles);
//
//		// profile
//		Account profile = new Account();
//		profile.setFirstname("Thinh");
//		profile.setLastname("Cam Tat");
////		profile.setEmail("thinh.tat@eztek.vn");
//		user.setAccount(profile);
//		return user;
//	}
//
//	public static User createUserWithoutEmail() {
//		User user = new User("thinhtat", "123123");
//		Role role1 = new Role();
//		role1.setRole("ROLE_USER");
//		Role role2 = new Role();
//		role2.setRole("ROLE_ADMIN");
//		Set<Role> roles = new HashSet<Role>();
//		roles.add(role1);
//		roles.add(role2);
//		// user.setRole(roles);
//
//		// profile
//		Account profile = new Account();
//		profile.setFirstname("Thinh");
//		profile.setLastname("Cam Tat");
//		// user.setProfile(profile);
//		return user;
//	}
//}
