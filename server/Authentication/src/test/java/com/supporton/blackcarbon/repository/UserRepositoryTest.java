package com.supporton.blackcarbon.repository;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.supporton.blackcarbon.auth.exception.NotFoundException;
import com.supporton.blackcarbon.auth.repository.AuthRepository;
import com.supporton.blackcarbon.auth.service.AuthenticateService;
import com.supporton.blackcarbon.model.auth.UserAuthentication;
import com.supporton.blackcarbon.repository.base.BaseTest;

/**
 * @author camthinh.tat
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRepositoryTest extends BaseTest {
	private static final String SQL_USER_CRIPTS = "user_data_script.sql";
	private static final String SQL_CLEAN_DATA_SCRIPT = "clean_data_script.sql";

	@Autowired
	private AuthRepository authRepository;
	
	@Autowired
	private AuthenticateService authService;

	@Before
	public void setUp() {
		executeSqlScript(SQL_USER_CRIPTS, false);
	}

	@After
	public void tearDown() throws SQLException {
		executeSqlScript(SQL_CLEAN_DATA_SCRIPT, false);
	}

	@Test
	public void testUser() {
		List<UserAuthentication> auths = authRepository.findAll();
		Assert.assertNotNull("User list should not be null", auths);

		Assert.assertTrue("Should be exist user", auths.size() == 1);
		Assert.assertTrue("Should be correct username", auths.get(0).getUsername().equals("admin"));
		Assert.assertTrue("Should be exist roles", auths.get(0).getRole().size() > 0);
	}
	
	@Test
	public void testAuthenticate() throws NotFoundException {
		String username = "admin";
		String password = "123123";
		
		UserAuthentication authentication = authService.authenticate(username, password);

		Assert.assertTrue("Should be exist user", authentication != null);
	}
}
