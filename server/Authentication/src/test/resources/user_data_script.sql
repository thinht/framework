use supporton;

INSERT INTO `roles` (id, name, description) VALUES (1, 'ROLE_SUPPER_ADMIN', 'super admin');
INSERT INTO `roles` (id, name, description) VALUES (2, 'ROLE_USER', 'user');
INSERT INTO `roles` (id, name, description) VALUES (3, 'ROLE_ADMIN', 'admin');
INSERT INTO `roles` (id, name, description) VALUES (4, 'ROLE_REPORTER', 'reporter');

INSERT INTO `users` (id, username, password, isActive) VALUES (1, 'admin', '$2a$10$TJleQPJO2OKKAzJHkbROjuy/4h2ARF.3Dgc4XJrp2jtylqZZhPgbK', true);

INSERT INTO `users_roles` (user_id, role_id) VALUES (1, 1);

