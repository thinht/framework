package com.supporton.blackcarbon.auth.exception;

/**
 * @author justinchasez
 *
 */
public class AlreadyExistException extends Exception {
  private static final long serialVersionUID = 1L;
}
