package com.supporton.blackcarbon.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.supporton.blackcarbon.model.auth.UserAuthentication;


/**
 * @author thinhtat
 * 
 */
public interface AuthRepository extends JpaRepository<UserAuthentication, Long> {
	UserAuthentication findByUsername(String username);
}
