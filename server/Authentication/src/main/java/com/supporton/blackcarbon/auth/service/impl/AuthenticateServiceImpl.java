package com.supporton.blackcarbon.auth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.supporton.blackcarbon.auth.exception.NotFoundException;
import com.supporton.blackcarbon.auth.repository.AuthRepository;
import com.supporton.blackcarbon.auth.service.AuthenticateService;
import com.supporton.blackcarbon.model.auth.UserAuthentication;

@Service
public class AuthenticateServiceImpl implements AuthenticateService {

	@Autowired
	private AuthRepository authRepository;

	@Override
	public UserAuthentication authenticate(String username, String password) throws NotFoundException {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String errorMessage = "Invalid username and password, please try again!";
		UserAuthentication userAuthentication = authRepository.findByUsername(username);
		CharSequence s = password;

		if (userAuthentication != null && passwordEncoder.matches(s, userAuthentication.getPassword())
				&& userAuthentication.getIsActive().booleanValue()) {
			return userAuthentication;
		} else {
			throw new NotFoundException(errorMessage);
		}
	}

}
