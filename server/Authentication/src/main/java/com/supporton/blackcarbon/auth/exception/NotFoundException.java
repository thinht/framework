package com.supporton.blackcarbon.auth.exception;

/**
 * @author camthinh.tat
 * 
 */
public class NotFoundException extends Exception {
  private static final long serialVersionUID = 1L;

  public NotFoundException(){

  }
  public NotFoundException(String messsage){
    super(messsage);

  }

}
