package com.supporton.blackcarbon.auth.service;

import com.supporton.blackcarbon.auth.exception.NotFoundException;
import com.supporton.blackcarbon.model.auth.UserAuthentication;

public interface AuthenticateService {
	UserAuthentication authenticate(String username, String password) throws NotFoundException;
}
